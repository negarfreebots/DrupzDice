<?php

namespace App\Console;
//namespace App\Notifications\diceToSlack;

use App\Console\Commands\SendNotification;
use App\Notifications\diceToSlack;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SendNotification::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->call(function(){
//            Log::info('something in one minute haha Taghir '.\Carbon\Carbon::now()->toDateTimeString());
//        })->everyMinute();
        $schedule->command('send:notification')->dailyAt('07:30')->skip(function () {
            $today = getdate();
            if($today['wday']==5) { // If it's Friday (wday 5), don't run the command!
                return true;
            }
            else {
                return false;
            }
        }); // 12:00 Tehran
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}

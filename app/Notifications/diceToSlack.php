<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class diceToSlack extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via()
    {
        return ['slack'];
    }


    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toSlack()
    {
        $index = rand(1 , 6);
        $names = [
            '1' => 'Negar',
            '2' => 'Sara',
            '3' => 'Farhad',
            '4' => 'Omid',
            '5' => 'Sadjad',
            '6' => 'Arsalan'
        ];

//        $mentions = [
//            '1' => 'negar',
//            '2' => 'sara',
//            '3' => 'fhd',
//            '4' => 'omidjm',
//            '5' => 'msadjad',
//            '6' => 'thearsalan'
//        ];

        $DrupzianName = $names[$index];
//        $DrupzianMention= $mentions[$index];

        return (new SlackMessage())
                ->success()
                ->from('Drupz Dice', ':drupz:')
                ->content("It's ".$DrupzianName."'s turn! \n");
    }
}
